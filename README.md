
# CDP6 Deliverable (HBP SGA1 M24 Review)

As previously demonstrated in our laboratory with the first de novo discovery of an allosteric site by in silico methods (Laine & al. PNAS (2010)), the analysis of the functional mechanism of the targeted protein and its associated cavities evolution can be a pertinent support for the design of novel effector ligands. Our work for CDP6 has been focused on the description of the activation mechanism of the (alpha7)Nicotinic Acetylcholine Receptors using innovative computational methods, coupled with the systematic analyze of protein cavities and grooves along the transition between the resting/active states, to identify novel protein sites presenting an allosteric lever on the channel gating. 

We initially modelled the receptor in an opened and closed channel states by comparative modelling using homologous X-ray crystallographic structures. We aimed to construct the most probable path of intermediate conformations between these two states. We used a combination of path optimization methods further tailored to address nicotinic receptors in their membrane environment (involving POE from Laine & al.2010, and string of swarm all-atom MD simulations in explicit water/ions/lipids). Successive iterations showed the spontaneous emergence of   quaternary motions along the transition, as admitted in the literature (Cecchini & Changeux, Neuropharm.,2015). Noticeably, we observed that the structural intermediates close to favorable Free Energy basins presented a blooming and twisting degree similar to those observed in the opened/closed GluCl and GLIC X-ray structures.

Original methods have been developed and assessed to globally identify and track cavities along the ion-channel activation (Monet et al., in preparation). Our transition model is made of 960 structures delineating the channel gating. We identified 68 sites consistently encompassing a cavity along the trajectory. Among them, 6 had a size and shape varying significantly with the conformational state of the protein. 5 were detected in the extracellular domain and 2 in the transmembrane domain of the receptor. Interestingly, among these selected pockets, we found the orthosteric site, the previously described Ca++ modulatory site (Le Novere & al., PNAS, 2001)  as well as a site in a similar locus than the Ivermectin binding site co-crystallized in the GluCl Receptor (Hibbs & al., Nature, 2011). We are currently performing a differential in silico screening of known allosteric modulators to probe the relevance of these pockets.

## Transition Path Analysis (a7)nAChR
Profiles of the blooming and twisting quartenary motion along the channel closure.

    transition_path/
        -- patha7m11-5-twist.dat
            Dihedral angle (degrees) of the 4 coordinates given by the arithmetic mean of the following atom selections:
                        (1) CA & ECD & subunit_i
                        (2) CA & ECD
                        (3) CA & TMD
                        (4) CA & TMD & subunit_i
            averaged over the 5 subunits of the receptor.

        -- patha7m11-5-blooming.dat
            Radius of gyration (Angstrom) of the atomic coordinates given by the selection (CA & ECD).

## Cavity tracking
Cavity tracking (software mkgridXf, Monet et al., in preparation) of the (a7)nAChR transition path trajectory (960 snapshots, receptor channel active/opened to resting/closed).

    cavity_tracking/
        -- frame001/cavities001.cor.zip
        -- frame002/cavities002.cor.zip
        ...
        -- frame960/cavities960.cor.zip

Cavity files can be unziped and visualized within the Visual Molecular Dynamics[1] software (CHARMM Coordinates format - Coloring Method:Resid, Drawing Method:Points).

## Cavity analysis

    cavity_analysis/
        -- cavity-volumes.dat
            Volumes (angstrom cubes) of the 68x5 individual cavities detected along the 960 snapshots of the transition.
        -- volumes_linregress.dat
	    Linear regression on cavity volumes. Sorted by absolute value of the corresponding slope.
        -- volumes_linregress-selected.dat
            Selected sites whose cavity size evolve with the channel state.
        -- selected-sites-volumes.agr
            xmgrace graphics of the selected sites cavity volumes along the channel closure.

## Differential virtual screening
Known (a7)nAChR effectors (orthosteric or allosteric) have been systematically docked on the previously selected sites to assess the relevance of the structural models.
Each compound is docked in the 960 conformations of each site using the virtual-screening program FlexX[2].

    dockings/
        -- docked_molecules.dat
            Compound name associated to the molecule identifiers in the docking files.
        -- docking_site_ID.zip
            Archive of the compounds poses for the site numbered ID:
            -- site-ID/frFRAME-cavID5-rad12-r8e.sdf
            ...
                Each file contains the poses for the corresponding FRAME, each poses intersecting the cavity ID5.


[1] Humphrey, W., Dalke, A. and Schulten, K., "VMD - Visual Molecular Dynamics", J. Molec. Graphics, 1996, vol. 14, pp. 33-38.

[2] LeadIT version 2.3.2; BioSolveIT GmbH, Sankt Augustin, Germany, 2017, www.biosolveit.de/LeadIT

This project has received funding from the European's Horizon 2020 Research and Innovation Programme under Grant Agreement No. 720270 (HBP SGA1).

